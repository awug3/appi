const SettingsTemplate = {props: [],
    data: () => ({
        radio: false,
        active: false,
        value: null,
        primary: [
                  'Orange',
                  'Apple',
                  'Pineapple'
                ],
        accent: [
                  'Cat',
                  'Dog',
                  'Rabbit'
                ],
        items: []
        }),
        methods: {
              goToProfile(){
              this.$router.push('mail_list');
              },
              goToAdd(){
                            this.$router.push('add');
                            }
        },
        mounted() { // when the Vue app is booted up, this is run automatically.
            var self = this // create a closure to access component in the callback below
            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail')
              .done(function( data ) {
              self.items = data.drinks;
              })
              .fail(function( jqxhr, textStatus, error ) {
               var err = textStatus + ", " + error;
               console.log( "Request Failed: " + err );
               });
          },
        template:`

        <div>
                <template>

                <div v-for="item in items">
                    <md-card>
                      <md-card-media>
                        <md-ripple>
                          <img v-bind:src="item.strDrinkThumb" alt="People">
                        </md-ripple>
                      </md-card-media>

                      <md-card-header>
                      <span class="md-title">{{item.strDrink}}</span>
                      </md-card-header>

                      <md-card-actions>
                        <md-button class="md-icon-button">
                          <md-icon>favorite</md-icon>
                        </md-button>

                        <md-button class="md-icon-button">
                          <md-icon>bookmark</md-icon>
                        </md-button>

                        <md-button class="md-icon-button">
                          <md-icon>share</md-icon>
                        </md-button>
                      </md-card-actions>
                    </md-card>
                    <br>
                </div>

                  </div>
                </template>
        </div>
`
                     };