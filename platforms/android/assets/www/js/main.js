    Vue.use(VueMaterial.default);
    Vue.use(VueRouter);

//    Vue.material.registerTheme('about', {
//          primary: {
//            color: 'indigo',
//            hue: 'A200'
//          },
//          accent: {
//            color: 'grey',
//            hue: 300
//          }
//        });

//Vue.material.setCurrentTheme('about');

const NotFound = { template: '<p>Page not found</p>' }
const Discover = { props: [],
                       data: () => ({
                           radio: false,
                           active: false,
                           value: null,
                           items: []
                           }),
                           methods: {
                                 goToProfile(){
                                 this.$router.push('mail_list');
                                 },
                                 goToAdd(){
                                               this.$router.push('add');
                                               }
                           },
                           mounted() { // when the Vue app is booted up, this is run automatically.
                               var self = this // create a closure to access component in the callback below
                               $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail')
                                 .done(function( data ) {
                                 self.items = data.drinks;
                                 })
                                 .fail(function( jqxhr, textStatus, error ) {
                                  var err = textStatus + ", " + error;
                                  console.log( "Request Failed: " + err );
                                  });
                             },
                           template:`

                           <div>
                                   <template md-flex>

                                   <div v-for="item in items" md-flex>
                                       <md-card md-with-hover>
                                         <md-card-media>
                                           <md-ripple>
                                             <img v-bind:src="item.strDrinkThumb" alt="People">
                                           </md-ripple>
                                         </md-card-media>

                                         <md-card-header>
                                         <span class="md-title">{{item.strDrink}}</span>
                                         </md-card-header>

                                         <md-card-actions>
                                           <md-button class="md-icon-button">
                                             <md-icon>favorite</md-icon>
                                           </md-button>

                                           <md-button class="md-icon-button">
                                             <md-icon>share</md-icon>
                                           </md-button>
                                         </md-card-actions>
                                       </md-card>
                                       <br>
                                   </div>

                                     </div>
                                   </template>
                           </div>
                   `
                                        };
const Cocktails = {

props: [],
    data: () => ({

        ingredients: [],
        categories: [],
        glasses: [],
        selectedIngredient: [],
        selectedCategory: [],
        selectedGlass: [],
        selectedAlcohol: [],
        alcohol: null
        }),

        methods: {
            search(x){
            var query;
            switch (x){
                case 'i':
                query = this.selectedIngredient;
                break;

                case 'c':
                query = this.selectedCategory;
                break;

                case 'g':
                query = this.selectedGlass;
                break;

                case 'a':
                query = this.selectedAlcohol;
                break;

                default:
                query = '';
                break;

            }

                $.ajax({
                    type: "get",
                    url: "https://www.thecocktaildb.com/api/json/v1/1/filter.php?"+x+"="+query,
                    success: function(result){
                    alert('hola '+x+' '+query);
                        }
                 });
            }
        },

        mounted() { // when the Vue app is booted up, this is run automatically.
            var self = this // create a closure to access component in the callback below
            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list')
              .done(function( data ) {
              self.ingredients = data.drinks;
              })
              .fail(function( jqxhr, textStatus, error ) {
               var err = textStatus + ", " + error;
               console.log( "Request Failed: " + err );
               });


            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list')
                             .done(function( data ) {
                             self.categories = data.drinks;
                             })
                             .fail(function( jqxhr, textStatus, error ) {
                              var err = textStatus + ", " + error;
                              console.log( "Request Failed: " + err );
                              });

            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list')
                                         .done(function( data ) {
                                         self.glasses = data.drinks;
                                         })
                                         .fail(function( jqxhr, textStatus, error ) {
                                          var err = textStatus + ", " + error;
                                          console.log( "Request Failed: " + err );
                                          });

          },

template:`
<form class="md-layout">
 <md-card class="md-layout-item md-size-50 md-small-size-100">
        <md-card-header>
          <div class="md-title">Buscar por ingredientes</div>
        </md-card-header>

        <md-card-content>

          <div class="md-layout md-gutter">
            <div class="md-layout-item md-small-size-100">

                <md-field id="ingredients">
                          <label for="ingredients">Ingredientes</label>
                          <md-select v-model="selectedIngredient" name="ingredients" id="ingredients2">
                            <div v-for="ingredient in ingredients">
                                 <md-option v-bind:value="ingredient.strIngredient1">{{ingredient.strIngredient1}}</md-option>
                            </div>

                          </md-select>
                        </md-field>


            </div>
          </div>


         <md-card-content>

          <md-card-actions>
                    <md-button type="submit" class="md-primary" @click="search('i')">  <md-icon>search</md-icon> Buscar</md-button>
                  </md-card-actions>

  </md-card>

  <md-card class="md-layout-item md-size-50 md-small-size-100">
                 <md-card-header>
                   <div class="md-title">Buscar por tipo de copa</div>
                 </md-card-header>

                 <md-card-content>
                    <div class="md-layout md-gutter">
                                           <div class="md-layout-item md-small-size-100">
                                                      <md-field>
                                                       <label for="category">Copas</label>
                                                        <md-select v-model="selectedGlass" name="glass" id="glass">
                                                                 <div v-for="glass in glasses">
                                                                   <md-option v-bind:value="glass.strGlass">{{glass.strGlass}}</md-option>
                                                                 </div>
                                                       </md-field>
                                             </div>
                     </div>


                 <md-card-content>

                  <md-card-actions>
                            <md-button type="submit" class="md-primary" @click="search('g')">  <md-icon>search</md-icon> Buscar</md-button>
                          </md-card-actions>

          </md-card>



  <md-card class="md-layout-item md-size-50 md-small-size-100">
           <md-card-header>
             <div class="md-title">Alcohol</div>
           </md-card-header>

           <md-card-content>

             <div class="md-layout md-gutter">
                                   <div class="md-layout-item md-small-size-100">
                                       <div class="row">
                                         <md-radio v-model="selectedAlcohol" value="null">Todos</md-radio>
                                         <md-radio v-model="selectedAlcohol" value="Alcoholic">Con alcohol</md-radio>
                                         <md-radio v-model="selectedAlcohol" value="Non_Alcoholic">Sin alcohol</md-radio>
                                         <md-radio v-model="selectedAlcohol" value="Optional_alcohol">Opcional</md-radio>
                                       </div>
                                   </div>
                       </div>



           <md-card-content>

            <md-card-actions>
                      <md-button type="submit" class="md-primary" @click="search('a')">  <md-icon>search</md-icon> Buscar</md-button>
                    </md-card-actions>

    </md-card>

    <md-card class="md-layout-item md-size-50 md-small-size-100">
               <md-card-header>
                 <div class="md-title">Buscar por categorías</div>
               </md-card-header>

               <md-card-content>

                 <div class="md-layout md-gutter">
                       <div class="md-layout-item md-small-size-100">
                                  <md-field>
                                   <label for="category">Categorías</label>
                                    <md-select v-model="selectedCategory" name="categories" id="categories">
                                             <div v-for="category in categories">
                                               <md-option v-bind:value="category.strCategory">{{category.strCategory}}</md-option>
                                             </div>
                                   </md-field>
                         </div>
                 </div>
               <md-card-content>

                <md-card-actions>
                          <md-button type="submit" class="md-primary" @click="search('c')">  <md-icon>search</md-icon> Buscar</md-button>
                        </md-card-actions>

        </md-card>
 </form>



                                    <style lang="scss" scoped>
                                      .md-checkbox {
                                        display: flex;
                                      }
                                      </style>

                                    `};

 const Home = {
 props: [],

 template:

 `
    <div style="background-image: url('img/background.jpg'); background-size: contain; height: 84.8vh">
  <div class="md-layout">
        <div class="md-layout-item" style="padding-top:33%; min-height:50%; color: #FFFFFF; text-align: center">
            <input type="text" style="width: 80%; height: 2.15rem; background-color: transparent; color: #FFFFFF">
        </div>
  </div>
     <md-field>
           <label>Initial Value</label>
           <md-input v-model="initial"></md-input>
         </md-field>
    </div>
  `

 };


 const MailListTemplate = {
    props: [],

 template:

 `
    <div style="background-image: url('img/background.jpg'); background-size: cover; height: 84.8vh">
  <div class="md-layout">
        <div class="md-layout-item" style="padding-top:33%; min-height:50%; color: #FFFFFF; text-align: center">
            <input type="text" style="width: 80%; height: 2.15rem; background-color: transparent; color: #FFFFFF">
        </div>
  </div>
     <md-field>
           <label>Initial Value</label>
           <md-input v-model="initial"></md-input>
         </md-field>
    </div>
  `
 };


function init(){

    createDB();
//    var fav = $('.fav');
//    fav.click(function(){
//    insertFav($(this).value);
//    };

    ///////////// DATABASE:  ///////////////////

    function createDB(){

         db = window.sqlitePlugin.openDatabase({name: 'favorites.db', location: 'default'});

         db.sqlBatch([
        'CREATE TABLE IF NOT EXISTS favorites (id,drink,image)',
         ], function() {
        console.log('Created database OK');
      }, function(error) {
        console.log('SQL batch ERROR: ' + error.message);
      });

    }



//    function insertFav(id){
//
//         var title;
//         var image;
//
//
//         var request = $.ajax({
//                  url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+id,
//                  method: "GET"
//                });
//
//                request.done(function( drink ) {
//
//                title = drink.drinks[0].strDrink;
//                image = drink.drinks[0].strDrinkThumb;
//
//                 db.executeSql('INSERT INTO favorites VALUES (?,?,?)', [id,title,image], function(rs) {
//                         alert("insertado!");
//
//                  }, function(error) {
//                      alert(" no insertado!" + error.message);
//
//                    console.log('SELECT SQL statement ERROR: ' + error.message);
//                  });
//
//                    });
//
//
//                request.fail(function( jqXHR, textStatus ) {
//                  alert( "Request failed: " + textStatus );
//            });
//
//
//
//
//    }


    
    const routes = [
            {path: '/home', name: 'home',  component: Home}
            ,{path: '/cocktails', name: 'cocktails', component: Cocktails},
            {path: '/discover', name: 'discover', component: Discover},
            {path: '/profile', name: 'profile', component: MailListTemplate}
            ];

        const router = new VueRouter({
                routes // short for `routes: routes`
            });

    const app = new Vue({
        el: '#app',
        router,
        data: { showNavigation: false,
                showSidepanel: false,
              message: 'Hola!'},
        methods: {
            goToProfile: function(){
//                this.showNavigation = false;
                    //this.$refs.sidebar.toggle();
                    router.push('mail_list');
                },
            goHome: function(){
//                this.showNavigation = false;
                    //this.$refs.sidebar.toggle();
                    router.push('home')
                },
            goToCocktails: function(){
                    //this.$refs.sidebar.toggle();
                    router.push('cocktails');
                },

            goToDiscover: function(){
                    router.push('discover');
            }
        }
            
      }).$mount('#app');
    
    //router.push('settings');
    router.push({ name: 'home'})
}

        

