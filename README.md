# Coctelarium (anteriormente Reciperium)

�Busca c�cteles por ingredientes!
Con Coctelarium, puedes buscar recetas de c�cteles. Hay dos tipos de b�squeda: por tags o por ingredientes.

# Componentes:
- Eric Serrulla
- Julian Ramos
- Jaume Mateo Cacheiro
- Sergi Rodriguez
- Alba Martines

# Apis Clave
- TheCocktailDB (https://www.thecocktaildb.com/)
