    Vue.use(VueMaterial.default);
    Vue.use(VueRouter);

//    Vue.material.registerTheme('about', {
//          primary: {
//            color: 'indigo',
//            hue: 'A200'
//          },
//          accent: {
//            color: 'grey',
//            hue: 300
//          }
//        });

//Vue.material.setCurrentTheme('about');



function init(){

    createDB();

    /////////////     DATABASE:  ///////////////////

    function createDB(){

         db = window.sqlitePlugin.openDatabase({name: 'favorites.db', location: 'default'});

         db.sqlBatch([
        'CREATE TABLE IF NOT EXISTS favorites (id,drink,image)',
         ], function() {
        console.log('Created database OK');
      }, function(error) {
        console.log('SQL batch ERROR: ' + error.message);
      });

    }


    
    const routes = [
            {path: '/home', name: 'home',  component: Home},
            {path: '/result', name: 'result', component: Result, props: true},
            {path: '/cocktails', name: 'cocktails', component: Cocktails},
            {path: '/results', name: 'results', component: Results, props: true},
            {path: '/discover', name: 'discover', component: Discover},
            {path: '/details', name: 'details', component: Details, props: true},
            {path: '/profile', name: 'profile', component: Profile}
            ];

        const router = new VueRouter({
                routes // short for `routes: routes`
            });

    const app = new Vue({
        el: '#app',
        router,
        data: { showNavigation: false,
                showSidepanel: false,
              message: 'Hola!'},
        methods: {
            insertFav: function(id){
            alert(id);
                newFav(id);
            },
            goHome: function(){
//                this.showNavigation = false;
                    //this.$refs.sidebar.toggle();
                    router.push('home')
                },
            goToCocktails: function(){
                    //this.$refs.sidebar.toggle();
                    router.push('cocktails');
                },

            goToDiscover: function(){
                    router.push('discover');
            },
            goToProfile: function(){
                    router.push('profile');
            }
        }
            
      }).$mount('#app');
    
    //router.push('settings');
    router.push({ name: 'home'})
}

        

