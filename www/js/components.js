const NotFound = { template: '<p>Page not found</p>' }
const Details = { props: ['id'],
                                       data: () => ({
                                           items: [],
                                           cocktail: {},
                                           id: this.id,
                                           loading: true
                                           }),
                                           methods: {
                                           insertFav(id){
                                                    $.ajax({
                                                       url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+id,
                                                       method: "GET",
                                                       success: function(drink){

                                                                       var title = drink.drinks[0].strDrink;
                                                                       var image = drink.drinks[0].strDrinkThumb;

                                                                        db.executeSql('INSERT INTO favorites VALUES (?,?,?)', [id,title,image], function(rs) {
                                                    console.log('+++++++++++++++++ rowsAdded: ' + rs.rowsAffected);



                                                                         }, function(error) {
                                                                             alert(" no insertado!" + error.message);

                                                                           console.log('SELECT SQL statement ERROR: ' + error.message);
                                                                         });

                                                                 }
                                                       });
                                            },

                                            goBack(){
                                            this.$router.push({name: 'discover'});
                                            },

                                            share(){
                                               window.plugins.socialsharing.share('Look at this cool cocktail! - via Coctelarium');
                                             }
                                           },
                                           mounted(){
                                           var self = this; // create a closure to access component in the callback below
                                                                               $.ajax({
                                                                                   method: "GET",
                                                                                   url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+self.id,
                                                                                   success: function (drink) {
                                                                                       self.cocktail = drink.drinks[0];
                                                                                       self.loading = false;
                                                                                   }
                                                                               });
                                           },
                                           template:`

                                           <div>
                                                   <template md-flex>
                                             <md-progress-bar v-if="loading" md-mode="indeterminate"></md-progress-bar>

                                                    <md-toolbar class="black-bar white">
                                                    <md-button @click="goBack()" class="md-icon-button">
                                                            <md-icon>arrow_back</md-icon>
                                                          </md-button>
                                                          <h4 class="caps">{{cocktail.strDrink}}</h4>
                                                     </md-toolbar>
                                                    <img v-bind:src="cocktail.strDrinkThumb">
                                                    <md-content class="md-elevation-4" style="    justify-content: center; padding:2rem;">

                                                    <span class="md-caption">{{cocktail.strCategory}} · </span>
                                                    <span class="md-caption">{{cocktail.strGlass}} · </span>
                                                    <span class="md-caption">{{cocktail.strAlcoholic}}</span>


                                                    <h4>Ingredients:</h4>
                                                    <ul>
                                                    <li v-if="cocktail.strIngredient1">{{cocktail.strIngredient1}}</li>
                                                    <li v-if="cocktail.strIngredient2">{{cocktail.strIngredient2}}</li>
                                                    <li v-if="cocktail.strIngredient3">{{cocktail.strIngredient3}}</li>
                                                    <li v-if="cocktail.strIngredient4">{{cocktail.strIngredient4}}</li>
                                                    <li v-if="cocktail.strIngredient5">{{cocktail.strIngredient5}}</li>
                                                    <li v-if="cocktail.strIngredient6">{{cocktail.strIngredient6}}</li>

                                                    </ul>

                                                    <h4>Instructions:</h4>
                                                      <p class="md-body-1">{{cocktail.strInstructions}}</p>
                                                    </md-content>



                                                   </template>
                                           </div>
                                   `
};

const Discover = { props: [],
                       data: () => ({
                           items: [],
                           detail: [],
                           details: false,
                           cocktail: {},
                           loading: true
                           }),
                           methods: {
                                 goToProfile(){
                                 this.$router.push('profile');
                                 },
                                 goToAdd(){
                                               this.$router.push('add');
                                 },
                                 insertFav(id){
                                    $.ajax({
                                       url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+id,
                                       method: "GET",
                                       success: function(drink){

                                                       var title = drink.drinks[0].strDrink;
                                                       var image = drink.drinks[0].strDrinkThumb;

                                                        db.executeSql('INSERT INTO favorites VALUES (?,?,?)', [id,title,image], function(rs) {
                                    console.log('+++++++++++++++++ rowsAdded: ' + rs.rowsAffected);



                                                         }, function(error) {
                                                             alert(" no insertado!" + error.message);

                                                           console.log('SELECT SQL statement ERROR: ' + error.message);
                                                         });

                                                           }
                                       });
                                 },

                                 share(){
                                    window.plugins.socialsharing.share('Look at this cool cocktail! - via Coctelarium');
                                 },

                                 showDetails(id){
                                     this.$router.push({ name: 'details', params: {id: id}})

                                 }
                           },
                           mounted() { // when the Vue app is booted up, this is run automatically.
                                var self = this; // create a closure to access component in the callback below
                                                          $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail')
                                                            .done(function( data ) {
                                                            self.items = data.drinks;
                                                            self.loading=false
                                                            })
                                                            .fail(function( jqxhr, textStatus, error ) {
                                                             var err = textStatus + ", " + error;
                                                             console.log( "Request Failed: " + err );
                                                             });
                             },
                           template:`

                           <div>


                                   <template md-flex>
                                   <div v-if="loading">
                                    <md-toolbar class="black-bar white">
                                             <h4>Finding the coolest cocktails...</h4>
                                      </md-toolbar>
                                     <md-progress-bar md-mode="indeterminate"></md-progress-bar>

                                   </div>

                                   <div v-for="item in items" md-flex>
                                       <md-card md-with-hover>
                                         <md-card-media>
                                           <md-ripple>
                                             <img @click="showDetails(item.idDrink)" v-bind:src="item.strDrinkThumb" alt="Drink">
                                           </md-ripple>
                                         </md-card-media>

                                         <md-card-header>
                                         <span @click="showDetails(item.idDrink)" class="md-title">{{item.strDrink}}</span>
                                         </md-card-header>

                                         <md-card-actions>
                                           <md-button @click="insertFav(item.idDrink)" class="md-icon-button">
                                             <md-icon>favorite</md-icon>
                                           </md-button>

                                           <md-button @click="share" class="md-icon-button">
                                             <md-icon>share</md-icon>
                                           </md-button>
                                         </md-card-actions>
                                       </md-card>
                                       <br>
                                   </div>

                                   </template>
                           </div>
                   `
};

const Cocktails = {

props: [],
    data: () => ({

        ingredients: [],
        categories: [],
        glasses: [],
        selectedIngredient: [],
        selectedCategory: [],
        selectedGlass: [],
        selectedAlcohol: [],
        alcohol: null,
        loading: true
        }),

        methods: {
            search(x){
            var query;
            switch (x){
                case 'i':
                query = this.selectedIngredient;
                break;

                case 'c':
                query = this.selectedCategory;
                break;

                case 'g':
                query = this.selectedGlass;
                break;

                case 'a':
                query = this.selectedAlcohol;
                break;

                default:
                query = '';
                break;

            }
            if (!query){
            alert('Introduce un valor por favor');
            }
            else {

            this.$router.push({ name: 'results', params: {query: x, value: query}})
            }

            }
        },

        mounted() { // when the Vue app is booted up, this is run automatically.
            var self = this; // create a closure to access component in the callback below
            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list')
              .done(function( data ) {
              self.ingredients = data.drinks;
              self.loading = false;
              })
              .fail(function( jqxhr, textStatus, error ) {
               var err = textStatus + ", " + error;
               console.log( "Request Failed: " + err );
               });


            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list')
                             .done(function( data ) {
                             self.categories = data.drinks;
                              self.loading = false;
                             })
                             .fail(function( jqxhr, textStatus, error ) {
                              var err = textStatus + ", " + error;
                              console.log( "Request Failed: " + err );
                              });

            $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list')
                                         .done(function( data ) {
                                         self.glasses = data.drinks;
                                         self.loading = false;
                                         })
                                         .fail(function( jqxhr, textStatus, error ) {
                                          var err = textStatus + ", " + error;
                                          console.log( "Request Failed: " + err );
                                          });

          },

    template:`
<div>
                     <md-progress-bar v-if="loading" md-mode="indeterminate"></md-progress-bar>

    <form class="md-layout">

        <md-card class="md-layout-item md-size-50 md-small-size-100">
            <md-card-header>
          <div class="md-title">Search by ingredients</div>
            </md-card-header>

            <md-card-content>

          <div class="md-layout md-gutter">
            <div class="md-layout-item md-small-size-100">

                <md-field id="ingredients">
                          <label for="ingredients">Ingredients</label>
                          <md-select v-model="selectedIngredient" name="ingredients" id="ingredients2">
                            <div v-for="ingredient in ingredients">
                                 <md-option v-bind:value="ingredient.strIngredient1">{{ingredient.strIngredient1}}</md-option>
                            </div>

                          </md-select>
                        </md-field>


            </div>
          </div>


            <md-card-content>

          <md-card-actions>
                    <md-button type="submit" class="md-primary" @click="search('i')">  <md-icon>search</md-icon> Search</md-button>
                  </md-card-actions>

  </md-card>

  <md-card class="md-layout-item md-size-50 md-small-size-100">
                 <md-card-header>
                   <div class="md-title">Search by glass type</div>
                 </md-card-header>

                 <md-card-content>
                    <div class="md-layout md-gutter">
                                           <div class="md-layout-item md-small-size-100">
                                                      <md-field>
                                                       <label for="category">Glasses</label>
                                                        <md-select v-model="selectedGlass" name="glass" id="glass">
                                                                 <div v-for="glass in glasses">
                                                                   <md-option v-bind:value="glass.strGlass">{{glass.strGlass}}</md-option>
                                                                 </div>
                                                       </md-field>
                                             </div>
                     </div>


                 <md-card-content>

                  <md-card-actions>
                            <md-button type="submit" class="md-primary" @click="search('g')">  <md-icon>search</md-icon> Search</md-button>
                          </md-card-actions>

          </md-card>



  <md-card class="md-layout-item md-size-50 md-small-size-100">
           <md-card-header>
             <div class="md-title">Alcohol</div>
           </md-card-header>

           <md-card-content>

             <div class="md-layout md-gutter">
                       <div class="md-layout-item md-small-size-100">
                                  <div class="row">
                                      <md-radio v-model="selectedAlcohol" value="Alcoholic">Alcoholic</md-radio>
                                      <md-radio v-model="selectedAlcohol" value="Non_Alcoholic">Non Alcoholic</md-radio>
                                      <md-radio v-model="selectedAlcohol" value="Optional_alcohol">Optional alcohol</md-radio>
                                  </div>
                       </div>
              </div>


           <md-card-content>

            <md-card-actions>
                      <md-button type="submit" class="md-primary" @click="search('a')">  <md-icon>search</md-icon> Search</md-button>
                    </md-card-actions>

    </md-card>

    <md-card class="md-layout-item md-size-50 md-small-size-100">
               <md-card-header>
                 <div class="md-title">Search by category</div>
               </md-card-header>

               <md-card-content>

                 <div class="md-layout md-gutter">
                       <div class="md-layout-item md-small-size-100">
                                  <md-field>
                                   <label for="category">Categories</label>
                                    <md-select v-model="selectedCategory" name="categories" id="categories">
                                             <div v-for="category in categories">
                                               <md-option v-bind:value="category.strCategory">{{category.strCategory}}</md-option>
                                             </div>
                                   </md-field>
                         </div>
                 </div>
               <md-card-content>

                <md-card-actions>
                          <md-button type="submit" class="md-primary" @click="search('c')">  <md-icon>search</md-icon> Search</md-button>
                        </md-card-actions>

        </md-card>
 </form>
 </div>



       <style lang="scss" scoped>
        .md-checkbox {
          display: flex;
         }
       </style>

                                    `
};

const Results = { props: ['query', 'value'],
                       data: () => ({
                           items: [],
                           query: this.query,
                           value: this.value,
                           loading: true,
                           error: false
                           }),
                           methods: {
                           insertFav(id){
                                    $.ajax({
                                       url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+id,
                                       method: "GET",
                                       success: function(drink){

                                                       var title = drink.drinks[0].strDrink;
                                                       var image = drink.drinks[0].strDrinkThumb;

                                                        db.executeSql('INSERT INTO favorites VALUES (?,?,?)', [id,title,image], function(rs) {
                                    console.log('+++++++++++++++++ rowsAdded: ' + rs.rowsAffected);



                                                         }, function(error) {
                                                             alert(" no insertado!" + error.message);

                                                           console.log('SELECT SQL statement ERROR: ' + error.message);
                                                         });

                                                           }
                                       });
                              },
                            goBack(){
                            this.$router.push({name: 'cocktails'});
                            },
                            share(){
                              window.plugins.socialsharing.share('Look at this cool cocktail! - via Coctelarium');
                            },
                            showDetails(id){
                                 this.$router.push({ name: 'details', params: {id: id}})

                            }
                           },
                           mounted() { // when the Vue app is booted up, this is run automatically.
                               var self = this; // create a closure to access component in the callback below
                               $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/filter.php?'+self.query+'='+self.value)
                                 .done(function( data ) {
                                 self.items = data.drinks;
                                 self.loading = false;
                                 self.error = true;
                                 })
                                 .fail(function( jqxhr, textStatus, error ) {
                                  var err = textStatus + ", " + error;
                                  console.log( "Request Failed: " + err );
                                  });
                             },
                           template:`

                           <div>
                                   <template md-flex>
                                <md-progress-bar v-if="loading" md-mode="indeterminate"></md-progress-bar>

<md-toolbar class="black-bar white">
                                    <md-button @click="goBack()" class="md-icon-button">
                                            <md-icon>arrow_back</md-icon>
                                          </md-button>
                                          <h4 v-if="loading">Searching for {{value}} drinks...</h4>
                                          <h4 v-if="!loading && items.length===0">No results found for {{value}} drinks</h4>
                                          <h4 v-if="!loading && items.length!==0" class="caps">Showing {{items.length}} {{value}} drinks</h4>
                                     </md-toolbar>


                                   <div v-for="item in items" md-flex>
                                       <md-card md-with-hover>
                                         <md-card-media>
                                           <md-ripple>
                                             <img @click="showDetails(item.idDrink)" v-bind:src="item.strDrinkThumb" alt="People">
                                           </md-ripple>
                                         </md-card-media>

                                         <md-card-header>
                                         <span @click="showDetails(item.idDrink)" class="md-title">{{item.strDrink}}</span>
                                         </md-card-header>

                                         <md-card-actions>
                                           <md-button @click="insertFav(item.idDrink)" class="md-icon-button">
                                             <md-icon>favorite</md-icon>
                                           </md-button>

                                           <md-button @click="share" class="md-icon-button">
                                             <md-icon>share</md-icon>
                                           </md-button>
                                         </md-card-actions>

                                       </md-card>
                                       <br>
                                   </div>

                                     </div>
                                   </template>
                           </div>
                   `
};

 const Home = {
    props: [],
    data: () => ({
          query: ''
    }),
    methods: {
        search(){
        var query = this.query;
        if (!query){
                    alert('Introduce un valor por favor');
                    }
                    else {

                    this.$router.push({ name: 'result', params: {value: query}});
                    }
        }
    },
    template:

 `
    <div class="md-layout md-gutter" style="background-image: url('img/background.jpg'); background-position: bottom; background-size: cover; height: 84.8vh">
            <div class="md-layout-item md-size-15"></div>

            <div class="md-layout-item white md-alignment-top-center" style="padding-top: 2rem;">
             <md-field class="white">
            <label class="grey caps"> Type a cocktail name</label>
            <md-input class="md-primary white" v-model="query" @keyup.enter="search"></md-input>
             <div @click="search"><md-icon>search</md-icon></div>

             </md-field>
                </div>
         <div class="md-layout-item md-size-15"></div>
    </div>
  `

 };

const Result = { props: ['value'],
                       data: () => ({
                           items: [],
                           query: this.query,
                           value: this.value,
                           loading: true,
                           error: false
                           }),
                           methods: {
                           insertFav(id){
                                    $.ajax({
                                       url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="+id,
                                       method: "GET",
                                       success: function(drink){

                                                       var title = drink.drinks[0].strDrink;
                                                       var image = drink.drinks[0].strDrinkThumb;

                                                        db.executeSql('INSERT INTO favorites VALUES (?,?,?)', [id,title,image], function(rs) {
                                    console.log('+++++++++++++++++ rowsAdded: ' + rs.rowsAffected);



                                                         }, function(error) {
                                                             alert(" no insertado!" + error.message);

                                                           console.log('SELECT SQL statement ERROR: ' + error.message);
                                                         });

                                                 }
                                       });
                            },

                            goBack(){
                            this.$router.push({name: 'home'});
                            },

                            share(){
                               window.plugins.socialsharing.share('Look at this cool cocktail! - via Coctelarium');
                             },

                             showDetails(id){
                                 this.$router.push({ name: 'details', params: {id: id}})
                             }
                           },
                           mounted() { // when the Vue app is booted up, this is run automatically.
                               var self = this; // create a closure to access component in the callback below
                               $.getJSON('https://www.thecocktaildb.com/api/json/v1/1/search.php?s='+self.value)
                                 .done(function( data ) {
                                 if (data.drinks){
                                      self.items = data.drinks;
                                 }
                                 self.loading = false;
                                 })
                                 .fail(function( jqxhr, textStatus, error ) {
                                  var err = textStatus + ", " + error;
                                  console.log( "Request Failed: " + err );
                                  error = true;
                                  });
                             },
                           template:`

                           <div>
                                   <template md-flex>

                                    <md-toolbar class="black-bar white">
                                    <md-button @click="goBack()" class="md-icon-button">
                                            <md-icon>arrow_back</md-icon>
                                          </md-button>
                                          <h4 v-if="loading">Searching for "{{value}}" drinks...</h4>
                                          <h4 v-if="!loading && items.length===0">No results found for <i>"{{value}}"</i></h4>
                                          <h4 v-if="!loading && items.length!==0" class="caps">Showing {{items.length}} "{{value}}" results</h4>
                                     </md-toolbar>
                     <md-progress-bar v-if="loading" md-mode="indeterminate"></md-progress-bar>

                     <div v-if="!loading && items.length===0 && !error" md-flex>
                                                                   <md-empty-state
                                                                     md-icon="sentiment_very_dissatisfied"
                                                                     md-label="No results found"
                                                                     md-description="Please try again with another search">
                                                                     <md-button @click="goBack" class="md-primary md-raised">Search...</md-button>
                                                                   </md-empty-state>
                      </div>

                      <div v-if="!loading && error" md-flex>
                                                                    <md-empty-state
                                                                      md-icon="sentiment_very_dissatisfied"
                                                                      md-label="Unexpected error"
                                                                     <md-button @click="goBack" class="md-primary md-raised">Try again</md-button>
                                                                     </md-empty-state>
                                            </div>

                                   <div v-for="item in items" md-flex>
                                       <md-card md-with-hover>
                                         <md-card-media>
                                           <md-ripple>
                                             <img @click="showDetails(item.idDrink)" v-bind:src="item.strDrinkThumb" alt="Drink">
                                           </md-ripple>
                                         </md-card-media>

                                         <md-card-header>
                                         <span @click="showDetails(item.idDrink)" class="md-title">{{item.strDrink}}</span>
                                         </md-card-header>

                                         <md-card-content>
                                         <div @click="showDetails(item.idDrink)">
                                            {{item.strInstructions}}
                                            </div>
                                         </md-card-content>

                                         <md-card-actions>
                                           <md-button @click="insertFav(item.idDrink)" class="md-icon-button">
                                             <md-icon>favorite</md-icon>
                                           </md-button>

                                           <md-button @click="share" class="md-icon-button">
                                             <md-icon>share</md-icon>
                                           </md-button>
                                         </md-card-actions>

                                       </md-card>
                                       <br>
                                   </div>

                                     </div>
                                   </template>
                           </div>
                   `
};


 const Profile = {
    props: [],
    data: () => ({
        items: []
        }),
    mounted() { // when the Vue app is booted up, this is run automatically.
                                   var self = this;
                                   db.executeSql('SELECT * FROM favorites', [], function(rs) {

                                    if (rs.rows.length !== 0){
                                            self.items = [];

                                        for(i=0;i<rs.rows.length;i++){
                                        self.items[i] = rs.rows.item(i);
                                        }
                                    }

                                   }, function(error) {
                                          console.log('SELECT SQL statement ERROR: ' + error.message);
});
    },

    methods: {
                deleteFav(id){
                    var self = this;
                    db.executeSql('DELETE FROM favorites WHERE id=?', [id], function(rs) {
                    console.log('+++++++++++++++++++ rowsDeleted: ' + rs.rowsAffected);

                    db.executeSql('SELECT * FROM favorites', [], function(rs) {

                                    if (rs.rows.length !== 0){
                                            self.items = [];

                                        for(i=0;i<rs.rows.length;i++){
                                        self.items[i] = rs.rows.item(i);
                                        }
                                    }

                                   }, function(error) {
                                          console.log('SELECT SQL statement ERROR: ' + error.message);
                        });
                    }, function(error) {
                       alert(" no eliminado!" + error.message);

                       console.log('SELECT SQL statement ERROR: ' + error.message);
                       });

                    },

                    goToDiscover(){
                      this.$router.push({name: 'discover'});
                    },

                    share(){
                       window.plugins.socialsharing.share('Look at this cool cocktail! - via Coctelarium');
                     },

                     showDetails(id){
                      this.$router.push({ name: 'details', params: {id: id}})
                      }
    },

 template:

 `
   <div>
                                 <template md-flex>

                                      <div v-if="items.length===0" md-flex>
                                              <md-empty-state
                                                md-icon="favorite_border"
                                                md-label="No favorites added"
                                                md-description="Here you'll see your favorite drinks and cocktails">
                                                <md-button @click="goToDiscover" class="md-primary md-raised">Start exploring</md-button>
                                              </md-empty-state>
                                      </div>

                                      <div v-else v-for="item in items" md-flex>

                                          <md-card md-with-hover>
                                            <md-card-media>
                                              <md-ripple>
                                                <img @click="showDetails(item.idDrink)" v-bind:src="item.image" alt="Drink">
                                              </md-ripple>
                                            </md-card-media>

                                            <md-card-header>
                                            <span @click="showDetails(item.idDrink)" class="md-title">{{item.drink}}</span>
                                            </md-card-header>

                                            <md-card-actions>
                                              <md-button @click="deleteFav(item.id)" class="md-icon-button md-accent">
                                                <md-icon>favorite</md-icon>
                                              </md-button>

                                              <md-button @click="share" class="md-icon-button">
                                                <md-icon>share</md-icon>
                                              </md-button>
                                            </md-card-actions>

                                          </md-card>
                                          <br>
                                      </div>

                                        </div>
                                 </template>
   </div>`
 };